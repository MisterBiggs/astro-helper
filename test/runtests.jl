using Test

import AstroHelper: Quaternion, Section,bending_moment, shear_stress, shear_flow, total_shear


@testset "AeroHelper" begin
 @testset "Quaternion Initialization" begin
    @test Quaternion().r == 1.0
    @test Quaternion([0,0,0,1]) == Quaternion()
    @test Quaternion([0 0 0], 0) == Quaternion()
    @test Quaternion([1 -2 2], (-2 * pi / 3)) ≈ Quaternion([-sqrt(3) / 6, sqrt(3) / 3, -sqrt(3) / 3, 1 / 2])

    # Roll of pi/2
    @test Quaternion(0, 0, pi / 2) ≈ Quaternion([1 0 0], pi / 2)
    
    @test_throws ErrorException Quaternion(1, 2, 3, 4)
 end

# @testset "Quaternion Math" begin
    # Quaternion Multiplication is not Communitive.
    # @test Quaternion() * Quaternion([0 1 0 0]) != Quaternion([0 1 0 0]) * Quaternion()
# end


    @testset "Section" begin
    tolerance = 1e-3
    complist(a, b) = abs(sum(a .- b)) < tolerance
    s = Section([2,1,0.5,0.5,0.75,0.75,2], [0,12,24,24,16,8,0], [0,0,0,6,7,8,9])
    @test all([s.Ixx == 126.075 s.Iyy == 571.2 s.Ixy ≈ -28.2])
    
    s = Section([1, .5, .5, 1], [-10, 20, 20, -10], [-5, -4, 4, 5])
    @test all([s.Ixx == 66 s.Iyy == 600 s.Ixy == 0])

    @test complist(bending_moment(s, -160000, -120000), [14121.2121 5696.9696 -13696.9696 -10121.2121])

    @test complist(shear_stress(s, 600, 800), [70.6061 14.2424 -34.2424 -50.6061])

    @test abs(shear_flow(s)[end]) < tolerance

end
end