struct Section
    data::DataFrame
    xc::Float64
    yc::Float64
    Ixx::Float64
    Iyy::Float64
    Ixy::Float64

    function Section(A, x, y)
        d = DataFrame(A=A, x=x, y=y)

        d.Ax = d.A .* d.x
        d.Ay = d.A .* d.y

        xc = sum(d.Ax) / sum(d.A)
        yc = sum(d.Ay) / sum(d.A)

        Ixx = sum(d.A .* ((d.y .- yc).^2))
        Iyy = sum(d.A .* ((d.x .- xc).^2))
        Ixy = sum(d.A .* ((d.x .- xc) .* (d.y .- yc)))


        new(d, xc, yc, Ixx, Iyy, Ixy)

    end 


end


function bending_moment(s::Section, Mx, My)
    s.data.σz = (My / s.Iyy) .* (s.data.x .- s.xc) + (Mx / s.Ixx) .* (s.data.y .- s.yc)

    s.data.σz
end

function shear_stress(s::Section, Vx, Vy)
    s.data.Δq = -(Vx / s.Iyy) * s.data.A .* (s.data.x .- s.xc) - (Vy / s.Ixx) * s.data.A .* (s.data.y .- s.yc)

    s.data.Δq
end


function shear_flow(s::Section)
    if !("Δq" in names(s.data))
        error("Missing Δq from Section! Run shear_stress function.")
    end

    q = s.data.Δq

    s.data.q = zeros(length(a))
    for (i, v) in enumerate(a)
		s.data.q[i] = sum(s.data.q) + v
	end
    s.data.q

end

function total_shear(s::Section, qo, thickness)
    s.data.τ = (s.q + qo) ./ thickness
    s.data.τ
end

export Section
