# AstroHelper

[![pipeline status](https://gitlab.com/MisterBiggs/astro-helper/badges/master/pipeline.svg)](https://gitlab.com/MisterBiggs/astro-helper/-/commits/master)


Functions to help with Astronautical Engineering Homework.

## Usage 

```julia
using Pkg; Pkg.add(url="https://gitlab.com/MisterBiggs/astro-helper"); using AstroHelper
```
